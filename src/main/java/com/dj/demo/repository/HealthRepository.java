package com.dj.demo.repository;

import com.dj.demo.domain.Health;
import org.springframework.data.repository.CrudRepository;

public interface HealthRepository extends CrudRepository<Health, Integer> {
}
