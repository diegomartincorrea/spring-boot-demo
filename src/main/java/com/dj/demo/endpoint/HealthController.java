package com.dj.demo.endpoint;

import com.dj.demo.repository.HealthRepository;
import com.dj.demo.domain.Health;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class HealthController {

    @Autowired
    private HealthRepository healthRepository;

    @RequestMapping("/invoke")
    public Health greeting(HttpServletRequest reques) {

        Health health = new Health(reques.getRemoteAddr());
        healthRepository.save(health);
        return health;
    }

    @RequestMapping(path="/invoke/list")
    public Iterable<Health> getAllUsers() {
        return healthRepository.findAll();
    }
}
